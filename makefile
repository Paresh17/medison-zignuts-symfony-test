.PHONY: all start stop bash frontend database migrate sleep

all: up sleep init frontend

up:
	@echo "Starting..."
	docker-compose up -d

stop:
	@echo "Stoping..."
	docker-compose down

bash:
	docker-compose exec medison-php-fpm /bin/bash

frontend:
	docker-compose exec medison-php-fpm /bin/bash -c "yarn encore dev --watch"

database:
	docker-compose exec medison-php-fpm /bin/bash -c "mysql -udev -p123 -hmedison-mariadb dev"

migrate:
	docker-compose exec medison-php-fpm /bin/bash -c "php bin/console doctrine:migrations:migrate --no-interaction"

init:
	docker-compose exec medison-php-fpm /bin/bash -c "composer install && php bin/console doctrine:database:drop --if-exists --force && php bin/console doctrine:database:create && php bin/console doctrine:migrations:migrate --no-interaction; php bin/console doctrine:fixtures:load --no-interaction"

sleep:
	sleep 5
