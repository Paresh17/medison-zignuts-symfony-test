# Change Log

## v2.0.0 - June 27, 2020

**Updated code**

1. Two times database query called for finding company job. so we stored that query result into variable. now query is calling only one time.

   ```
   Method name : company()
   File name : JobController.php
   Path : source/src/Controller
   ```

2. `Slugger.php` is already defined inside Utils folder, but when creating/updating job, `createSlug` function is not used. so now when generating any slug, we used that function.

   ```
   Method name : new() and edit()
   File name : JobController.php
   Path : source/src/Controller
   ```

3. When creating/updating job, same email code is written two times. so we created one more utility inside Utils folder(Mailer.php). for sending job email, we used job `jobEmail` method. we can define more function for other type of emails. also `from` and `to` emails are come from `.env`, so it is better for changing email from .env rather than changing from main code.

   ```
   Method name : jobEmail()
   File name : Mailer.php
   Path : source/src/Utils
   ```

4. Once user create draft job, there is no option for edit draft job. so created one more route `/draft` for draft jobs and linked to home page.

   ```
   Method name : draft()
   File name : JobController.php
   Path : source/src/Controller
   ```

5. Added file `_table.html.twig` for common tables (for active job, draft jobs, company)

   ```
   File name : _table.html.twig
   Path : source/templates/job
   ```

6. Updated styling for all pages. added default `bootstrap_4_layout.html.twig`(source/config/packages/twig.yaml) for form builder.

7. Added [PHPUnit](https://phpunit.de/getting-started/phpunit-7.html) for unit testing. you can install it by composer or run `composer update` command inside source folder.

8. Created `JobTest.php` inside tests directory. added one test for creating draft and unpaid job. you can create more functions for testing more features. For checking test is ok or not, go to `source` directory and run `./vendor/bin/phpunit`.

## v1.0.0 - June 20, 2020

**Fixed Bugs**

1.  Created infinite job, end date was null. but when updated it, end date became **2099-12-31**.

    ```
    Method name : edit(Request $request, Job $job, MailerInterface \$mailer)
    File name : JobController.php
    Path : source/src/Controller

    ```

2.  Created job with infinite checked and when edited that job, checkbox became **unchecked**.

    ```
    Method name : edit(Request $request, Job $job, MailerInterface \$mailer)
    File name : JobController.php
    Path : source/src/Controller

    ```
