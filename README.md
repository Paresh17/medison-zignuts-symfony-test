**Version 2.0.0** - [Change Log](CHANGELOG.md#v200-june-27-2020)

**Version 1.0.0** - [Change Log](CHANGELOG.md#v100-june-20-2020)

# Medison Test Quiz

This "Quiz" consists of a simple CRUD application to create Job Advertisments.  
The homepage will list all the job ads sorted by start date, that are active (`start <= today and (either end >= today or no end date) and draft = 0`). You can click on them to see their details. You can also click on the "Company", and see a list of all job ads this company has ever had.

**Your Job:** There is at least 1 bug in the code, and the code could use some refactoring in a few places. Your job is to refactor it and compose a list of what you did and why. There are no unit test (or any kind of tests) yet, please write them as you see fit, we're going for pragmatic tests, meaning you don't need to have 100% test coverage but at least for all found bugs and other functions you find critical/important! Feel free to add additonal services to `docker-compose.yml` as you see fit. The used `.env` file can be found in `config/.env`. The used DB Params are:

- username: `dev`
- password: `123`
- database: `dev`

**Questions:** Feel free to contact [pascal.wacker@medison.ch](mailto:pascal.wacker@medison.ch)

## Commands

- **start:** `docker-compose up -d && docker-compose exec medison-php-fpm /bin/bash -c "yarn encore dev --watch"`
- **stop:** `docker-compose down`
- **bash:** `docker-compose exec medison-php-fpm /bin/bash`
- **frontend:** `docker-compose exec medison-php-fpm /bin/bash -c "yarn encore dev --watch"` (included in the start command)
- **database:** `docker-compose exec medison-php-fpm /bin/bash -c "mysql -udev -p123 -hmedison-mariadb dev"`
- **migrate:** `docker-compose exec medison-php-fpm /bin/bash -c "php bin/console doctrine:migrations:migrate --no-interaction --quiet"`
- **init:** `docker-compose exec medison-php-fpm /bin/bash -c "php bin/console doctrine:fixtures:load --purge-with-truncate --no-interactive"`

### Makefile

- `make` runs start, migrate, init and frontend
- `make <command>` runs the command, ex. `make bash` or `make database`

## Ports

- `http://localhost:80` website
- `http://localhost:8080` frontend assets
- `http://localhost:8025` mailhog

## First Run

You can either use `make`, which will run these steps automatically or run them manually

1. Connect to Bash
2. Run Composer: `composer install`
3. Run DB Migrations: `php bin/console doctrine:migrations:migrate`
4. Run Fixtures: `php bin/console doctrine:fixtures:load --purge-with-truncate --no-interactive`
