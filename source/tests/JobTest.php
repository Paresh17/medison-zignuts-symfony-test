<?php
use App\Entity\Job;
use App\Utils\Slugger;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Persistence\ObjectManager;

class JobTest extends TestCase
{
   
    /**
     * create unpaid and draft job with no end date 
     */
    public function testCreateUnpaidAndDraftJob()
    {
        //Input parameters
        $title = "Test Darft Job";
        $company = "Test Company";
        $draft = true;
        $paid = false;
        $start_date = date('Y-m-d');
        
        //Create new Job
        try {
            $job = new Job();
            $slugger = new Slugger();
            $job->setTitle = $title;
            $job->setSlug = $slugger->createSlug($title);
            
            $job->setCompany = $company;
            $job->setCompanySlug = $slugger->createSlug($company);
            
            $job->setDraft = $draft;
            $job->setPaid = $paid;
            $job->setStart = $start_date;

            $objectManager = $this->createMock(ObjectManager::class);
            
            $objectManager->persist($job);
            $objectManager->flush();

            echo "\n\n Test Success \n";
            $this->assertTrue(true);
        
        } catch (\Throwable $th) {
            echo "\n\n Error : " . $th->getMessage()." \n";
            $this->assertTrue(false);
        }
    }
}