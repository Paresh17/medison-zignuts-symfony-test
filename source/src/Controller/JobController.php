<?php

namespace App\Controller;

use App\Entity\Job;
use App\Form\JobType;
use App\Utils\Mailer;
use App\Utils\Slugger;
use App\Repository\JobRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/")
 */
class JobController extends AbstractController
{
    /**
     * @Route("/", name="job_index", methods={"GET"})
     */
    public function index(JobRepository $jobRepository): Response
    {
        return $this->render('job/index.html.twig', [
            'jobs' => $jobRepository->findOnlineJobs(),
            'draft' => true,
        ]);
    }

    /**
     * @Route("/draft", name="job_draft", methods={"GET"})
     */
    public function draft(JobRepository $jobRepository): Response
    {
        return $this->render('job/draft.html.twig', [
            'jobs' => $jobRepository->findDraftJobs(),
        ]);
    }

    /**
     * @Route("/new", name="job_new", methods={"GET","POST"})
     */
    public function new(Request $request, Mailer $mailer, Slugger $slugger): Response
    {
        $job = new Job();
        $form = $this->createForm(JobType::class, $job, ['attr' => ['id' => 'job-form']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('infinite')->getData()) {
                $job->setEnd(null);
            } elseif (!$form->get('end')->getData()) {
                $this->addFlash('warning', 'No End Date given and Infinit not active, assume infinite!');
            }
            $job->setSlug($slugger->createSlug($job->getTitle()));
            $job->setCompanySlug($slugger->createSlug($job->getCompany()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job);
            $entityManager->flush();

            if (!$job->getDraft()) {
                $this->addFlash('success', 'Created new Job entry, this will cost CHF 490.-');
                $mailer->jobEmail('New Bill', '<p>Job published, bill for CHF 490.-  attached</p>');
            }

            return $this->redirectToRoute('job_index');
        }

        return $this->render('job/new.html.twig', [
            'job' => $job,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="job_show", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function show(Job $job): Response
    {
        return $this->render('job/show.html.twig', [
            'job' => $job,
        ]);
    }

    /**
     * @Route("/{slug}", name="job_detail", methods={"GET"})
     */
    public function detail(Job $job): Response
    {
        return $this->render('job/show.html.twig', [
            'job' => $job,
        ]);
    }

    /**
     * @Route("/company/{companySlug}", name="job_company", methods={"GET"})
     */
    public function company(Job $job, JobRepository $jobRepository): Response
    {
        $counts = [
            'draft' => 0,
            'paid' => 0,
        ];
        $jobs = $jobRepository->findCompanyJobs($job->getCompanySlug());
        
        foreach ($jobs as $job) {
            if ($job->getDraft()) {
                $counts['draft']++;
            }
            if ($job->getPaid()) {
                $counts['paid']++;
            }
        }

        return $this->render('job/index.html.twig', [
            'jobs' => $jobs,
            'company' => $job->getCompany(),
            'counts' => $counts,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="job_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Job $job, Mailer $mailer, Slugger $slugger): Response
    {
        $form = $this->createForm(JobType::class, $job, ['attr' => ['id' => 'job-form']]);
        if(!$job->getEnd()) {
            $form->get('infinite')->setData(true);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('infinite')->getData()) {
                $job->setEnd(null);
            } elseif (!$form->get('end')->getData()) {
                $this->addFlash('warning', 'No End Date given and Infinit not active, assume infinite!');
            }
            $job->setSlug($slugger->createSlug($job->getTitle()));
            $job->setCompanySlug($slugger->createSlug($job->getCompany()));
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Edited Job entry');

            if (!$job->getDraft()) {
                $mailer->jobEmail('New Bill', '<p>Job published, bill for CHF 490.-  attached</p>');
            }

            return $this->redirectToRoute('job_index');
        }
        return $this->render('job/edit.html.twig', [
            'job' => $job,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="job_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Job $job): Response
    {
        if ($this->isCsrfTokenValid('delete'.$job->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($job);
            $entityManager->flush();

            $this->addFlash('success', 'Removed Job entry');
        }

        return $this->redirectToRoute('job_index');
    }
}
