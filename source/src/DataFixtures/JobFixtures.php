<?php

namespace App\DataFixtures;

use App\Entity\Job;
use App\Utils\Slugger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Serializer\SerializerInterface;

class JobFixtures extends Fixture
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var Generator */
    private $faker;

    /** @var Slugger */
    private $slugger;

    /**
     * @required
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @required
     */
    public function setFaker()
    {
        $this->faker = Factory::create('de_CH');
    }

    /**
     * @required
     */
    public function setSlugger()
    {
        $this->slugger = new Slugger();
    }

    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=3; $i++) {
            $amount = $i*25;
            $company = $this->faker->company;
            for ($j=0; $j<$amount; $j++) {
                $job = new Job();
                $job->setTitle($this->faker->jobTitle . ' ' . $this->faker->jobTitle . ' ' . $this->faker->jobTitle);
                $job->setSlug($this->slugger->createSlug($job->getTitle()));
                $job->setCompany($company);
                $job->setCompanySlug($this->slugger->createSlug($job->getCompany()));
                $job->setDraft($j%7 === 0);
                $job->setPaid($j%3 === 0);
                if ($j%5 === 0) {
                    $job->setStart($this->faker->dateTimeBetween('+1 week', '+3 months'));
                } else {
                    $job->setStart($this->faker->dateTimeBetween('-3 months'));
                }
                if ($j%3 === 0) {
                    $job->setEnd(null);
                } elseif ($j%4 === 0) {
                    $job->setEnd($this->faker->dateTimeBetween('-3 months'));
                } else {
                    $job->setEnd($this->faker->dateTimeBetween('+1 week', '+3 months'));
                }
                $manager->persist($job);
            }
        }

        $manager->flush();
    }
}
