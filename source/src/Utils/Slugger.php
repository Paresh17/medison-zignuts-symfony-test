<?php

namespace App\Utils;

class Slugger
{
    public function createSlug(string $input): string
    {
        return preg_replace(['/[^A-Za-z0-9]/', '/\-\-+/'], ['-', '-'], strtolower($input));
    }
}
