<?php

namespace App\Utils;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use App\Entity\Job;

class Mailer
{
    /** @var Mailer */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function jobEmail($subject, $content)
    {
        $from_email =  $_ENV['FROM_EMAIL'] ?? 'bill@example.com';
        $to_email =  $_ENV['TO_EMAIL'] ?? 'you@example.com';
        $email = (new Email())
            ->from($from_email)
            ->to($to_email)
            ->subject($subject)
            ->html($content);
        $this->mailer->send($email);
    }
}
