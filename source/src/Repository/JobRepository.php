<?php

namespace App\Repository;

use App\Entity\Job;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Job::class);
    }

    /**
     * @return Job[]
     */
    public function findOnlineJobs(): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.start <= :now')
            ->andWhere('j.end >= :now')
            ->orWhere('j.end IS NULL')
            ->andWhere('j.draft = 0')
            ->setParameter('now', new \DateTime('today 00:00'))
            ->orderBy('j.paid', 'DESC')
            ->addOrderBy('j.start', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Job[]
     */
    public function findCompanyJobs(string $slug): array
    {
        return $this->createQueryBuilder('j')
            ->where('j.companySlug = :slug')
            ->setParameter('slug', $slug)
            ->orderBy('j.start', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Job[]
     */
    public function findDraftJobs(): array
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.draft = 1')
            ->orderBy('j.paid', 'DESC')
            ->addOrderBy('j.start', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Job[] Returns an array of Job objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Job
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
